/* ===================================================================
    
    Author          : Valid Theme
    Template Name   : Edukat - Education and LMS Template
    Version         : 1.0
    
* ================================================================= */


$(document).ready(function() {
    $('.navbar-toggler').click(function() {
        $('.navbar-collapse').slideToggle(300);
    });

    smallScreenMenu();
    let temp;

    function resizeEnd() {
        smallScreenMenu();
    }

    $(window).resize(function() {
        clearTimeout(temp);
        temp = setTimeout(resizeEnd, 100);
        resetMenu();
    });
});


const subMenus = $('.sub-menu');
const menuLinks = $('.menu-link');

function smallScreenMenu() {
    if ($(window).innerWidth() <= 992) {
        menuLinks.each(function(item) {
            $(this).click(function() {
                $(this).next().slideToggle();
            });
        });
    } else {
        menuLinks.each(function(item) {
            $(this).off('click');
        });
    }
}

function resetMenu() {
    if ($(window).innerWidth() > 992) {
        subMenus.each(function(item) {
            $(this).css('display', 'none');
        });
    }
}






function logo(img) {
    // document.getElementById('logo');
    var img = document.getElementById('logo');
    //   if (img.src.indexOf('logo')==-1) return; 
    var d = new Date();
    var Today = d.getDate();
    var Month = d.getMonth() + 1;
    var src = "https://aspireitacademy.in/assets/img/blogos/";
    // debugger;
    if (Month === 12) {
        src += "logos1.png";
    } else if (Month === 9) {
        src += "logos2.png";
    } else if (Month === 4 && (Today >= 12 && Today <= 16)) {
        src += "logos3.png";
    } else if (Month === 4 && (Today >= 2 && Today <= 6)) {
        src += "logos4.png";
    } else if (Month === 3 && (Today >= 16 && Today <= 19)) {
        src += "logos5.png";
    } else if (Month === 1) {
        src += "logos6.png";
    } else if (Month === 7 && (Today >= 20 && Today <= 23)) {
        src += "logos7.png";
    } else if (Month === 2 && (Today >= 13 && Today <= 15)) {
        src += "logos8.png";
    } else if (Month === 11 && (Today >= 3 && Today <= 5)) {
        src += "logos8.png";

    } else {
        src += "logo.png";
    }
    //   alert(src);
    img.src = src;
}



(function($) {
    "use strict";



    $(document).ready(function() {
        $(".dropdown").hover(
            function() {
                $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("400");
                $(this).toggleClass('open');
            },
            function() {
                $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("400");
                $(this).toggleClass('open');
            }
        );
    });


    $(document).on('ready', function() {


        /* ==================================================
            # Wow Init
         ===============================================*/
        var wow = new WOW({
            boxClass: 'wow', // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0, // distance to the element when triggering the animation (default is 0)
            mobile: true, // trigger animations on mobile devices (default is true)
            live: true // act on asynchronously loaded content (default is true)
        });
        wow.init();


        /* ==================================================
            # Tooltip Init
        ===============================================*/
        $('[data-toggle="tooltip"]').tooltip();


        /* ==================================================
            # Smooth Scroll
         ===============================================*/
        $("body").scrollspy({
            target: ".navbar-collapse",
            offset: 200
        });
        $('a.smooth-menu').on('click', function(event) {
            var $anchor = $(this);
            var headerH = '75';
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - headerH + "px"
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });


        /* ==================================================
            # Banner Animation
        ===============================================*/
        function doAnimations(elems) {
            //Cache the animationend event in a variable
            var animEndEv = 'webkitAnimationEnd animationend';
            elems.each(function() {
                var $this = $(this),
                    $animationType = $this.data('animation');
                $this.addClass($animationType).one(animEndEv, function() {
                    $this.removeClass($animationType);
                });
            });
        }

        //Variables on page load
        var $immortalCarousel = $('.animate_text'),
            $firstAnimatingElems = $immortalCarousel.find('.item:first').find("[data-animation ^= 'animated']");
        //Initialize carousel
        $immortalCarousel.carousel();
        //Animate captions in first slide on page load
        doAnimations($firstAnimatingElems);
        //Other slides to be animated on carousel slide event
        $immortalCarousel.on('slide.bs.carousel', function(e) {
            var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
            doAnimations($animatingElems);
        });


        /* ==================================================
            # Youtube Video Init
         ===============================================*/
        $('.player').mb_YTPlayer();


        /* ==================================================
            # imagesLoaded active
        ===============================================*/
        $('#portfolio-grid,.blog-masonry').imagesLoaded(function() {

            /* Filter menu */
            $('.mix-item-menu').on('click', 'button', function() {
                var filterValue = $(this).attr('data-filter');
                $grid.isotope({
                    filter: filterValue
                });
            });

            /* filter menu active class  */
            $('.mix-item-menu button').on('click', function(event) {
                $(this).siblings('.active').removeClass('active');
                $(this).addClass('active');
                event.preventDefault();
            });

            /* Filter active */
            var $grid = $('#portfolio-grid').isotope({
                itemSelector: '.pf-item',
                percentPosition: true,
                masonry: {
                    columnWidth: '.pf-item',
                }
            });

            /* Filter active */
            $('.blog-masonry').isotope({
                itemSelector: '.blog-item',
                percentPosition: true,
                masonry: {
                    columnWidth: '.blog-item',
                }
            });

        });


        /* ==================================================
            # Fun Factor Init
        ===============================================*/
        $('.timer').countTo();
        $('.fun-fact').appear(function() {
            $('.timer').countTo();
        }, {
            accY: -100
        });


        /* ==================================================
            Nice Select Init
         ===============================================*/
        $('select').niceSelect();


        /* ==================================================
            Countdown Init
         ===============================================*/
        loopcounter('counter-class');


        /* ==================================================
            # Magnific popup init
         ===============================================*/
        $(".popup-link").magnificPopup({
            type: 'image',
            // other options
        });

        $(".popup-gallery").magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            },
            // other options
        });

        $(".popup-youtube, .popup-vimeo, .popup-gmaps").magnificPopup({
            type: "iframe",
            mainClass: "mfp-fade",
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });

        $('.magnific-mix-gallery').each(function() {
            var $container = $(this);
            var $imageLinks = $container.find('.item');

            var items = [];
            $imageLinks.each(function() {
                var $item = $(this);
                var type = 'image';
                if ($item.hasClass('magnific-iframe')) {
                    type = 'iframe';
                }
                var magItem = {
                    src: $item.attr('href'),
                    type: type
                };
                magItem.title = $item.data('title');
                items.push(magItem);
            });

            $imageLinks.magnificPopup({
                mainClass: 'mfp-fade',
                items: items,
                gallery: {
                    enabled: true,
                    tPrev: $(this).data('prev-text'),
                    tNext: $(this).data('next-text')
                },
                type: 'image',
                callbacks: {
                    beforeOpen: function() {
                        var index = $imageLinks.index(this.st.el);
                        if (-1 !== index) {
                            this.goTo(index);
                        }
                    }
                }
            });
        });


        /* ==================================================
            # Feature Carousel
         ===============================================*/
        $('.feature-carousel').owlCarousel({
            loop: true,
            nav: false,
            margin: 30,
            dots: true,
            autoplay: true,
            items: 1,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            responsive: {
                1000: {
                    stagePadding: 100,
                }
            }
        });


        /* ==================================================
            # Cateogries Carousel
         ===============================================*/
        $('.categories-carousel').owlCarousel({
            loop: false,
            margin: 30,
            nav: false,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            dots: true,
            autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 2
                }
            }
        });

        /* ==================================================
            # Cateogries Carousel Two
         ===============================================*/
        $('.thumb-categories-carousel').owlCarousel({
            loop: true,
            nav: false,
            margin: 30,
            dots: false,
            autoplay: true,
            items: 1,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                800: {
                    items: 2
                },
                1000: {
                    items: 3,
                    stagePadding: 100,
                }
            }
        });


        /* ==================================================
            # Testimonials Carousel
         ===============================================*/
        $('.testimonials-carousel').owlCarousel({
            loop: true,
            nav: false,
            margin: 30,
            dots: true,
            autoplay: false,
            items: 1,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 2,
                }
            }
        });


        /* ==================================================
            # Advisor Carousel
         ===============================================*/
        $('.advisor-carousel').owlCarousel({
            loop: true,
            nav: false,
            margin: 30,
            dots: true,
            autoplay: false,
            items: 1,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3,
                }
            }
        });


        /* ==================================================
            # Courses Carousel
         ===============================================*/
        $('.courses-carousel').owlCarousel({
            loop: true,
            nav: true,
            margin: 30,
            dots: false,
            autoplay: false,
            items: 1,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 3,
                }
            }
        });


        /* ==================================================
            # Partner Carousel
         ===============================================*/
        $('.partner-carousel').owlCarousel({
            loop: true,
            nav: false,
            margin: 80,
            dots: false,
            autoplay: false,
            items: 1,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            responsive: {
                0: {
                    items: 2,
                    margin: 30
                },
                600: {
                    items: 3,
                    margin: 30
                },
                1000: {
                    items: 5,
                }
            }
        });


        /* ==================================================
            Preloader Init
         ===============================================*/
        $(window).on('load', function() {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");;
        });


        /* ==================================================
            Contact Form Validations
        ================================================== */
        $('.contact-form').each(function() {
            var formInstance = $(this);
            formInstance.submit(function() {

                var action = $(this).attr('action');

                $("#message").slideUp(750, function() {
                    $('#message').hide();

                    $('#submit')
                        .after('<img src="assets/img/ajax-loader.gif" class="loader" />')
                        .attr('disabled', 'disabled');

                    $.post(action, {
                            name: $('#name').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            comments: $('#comments').val()
                        },
                        function(data) {
                            document.getElementById('message').innerHTML = data;
                            $('#message').slideDown('slow');
                            $('.contact-form img.loader').fadeOut('slow', function() {
                                $(this).remove()
                            });
                            $('#submit').removeAttr('disabled');
                        }
                    );
                });
                return false;
            });
        });

    });


    // When the user scrolls the page, execute myFunction
    window.onscroll = myFunction
        // Get the navbar
    var navbar = document.getElementById("navbar");

    // Get the offset position of the navbar
    var sticky = navbar.offsetTop;

    // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky");

        } else {
            navbar.classList.remove("sticky");

        }
    } // end document ready function

    /* ==================================================
                Whatsapp chat
            ================================================== */

    var whatsappNos = [{
        phone: "919778595719",
        counsellor_name: "Susmitha"
    }, {
        phone: "918921256881",
        counsellor_name: "Avani"
    }, {
        phone: "917510593767",
        counsellor_name: "Anu Mohan"
    }]

    function openWhatsapp() {
        var selected = whatsappNos[Math.floor(Math.random() * whatsappNos.length)];
        var selected_number = selected.phone;
        var selected_counsellor = selected.counsellor_name;
        var d = new Date,
            date = [d.getMonth() + 1,
                d.getDate(),
                d.getFullYear()
            ].join('/') + ' ' + [d.getHours(),
                d.getMinutes(),
                d.getSeconds()
            ].join(':');
        var url = "";
        if ($(window).width() < 520) {
            //Show mobile link
            url = "https://api.whatsapp.com/send/?phone=" + selected_number + "&text&app_absent=0";
        } else {
            //Show Web link
            url = "https://api.whatsapp.com/send/?phone=" + selected_number + "&text&app_absent=0";
        }
        //var url = "https://api.whatsapp.com/send/?phone=" + selected_number + "&text&app_absent=0";
        window.open(url, '_blank');
        $.ajax({
            url: 'whatsappchatcounter.php',
            data: {
                "counsellor_name": selected.counsellor_name,
                "date": date

            },
            method: 'POST',
            success: function(data) {},
            error: function() {

            }
        });

    }

    window.openWhatsapp = openWhatsapp;

})(jQuery); // End jQuery